import 'package:flutter/material.dart';

void main() {
  var app = MaterialApp(
    theme: ThemeData(
      primaryColor: Colors.lightBlue.shade300,
    ),
    home: Scaffold(
      appBar: AppBar(),
      body: Center(child: Text("Hello World", textScaleFactor: 2,)),
    )
  );

  runApp(app);
}

